import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { setTheme } from '../../../reducer/commonSlice';
import styles from './ThemeSwitch.module.scss';

function ThemeSwitch() {
    const store = useSelector((state) => state.common);
    const { theme } = store;
    const dispatch = useDispatch();

    useEffect(() => {
        const theme = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';
        dispatch(setTheme(theme));
    }, [dispatch]);

    useEffect(() => {
        updateRootThemeClass();
    });

    function updateRootThemeClass() {
        if (theme === 'light') {
            document.documentElement.classList.remove('dark');
            return;
        }
        // 以light為基礎開發，先註解掉
        // document.documentElement.classList.add('dark');
    }

    function handleChange(e) {
        dispatch(setTheme(e.target.checked ? 'light' : 'dark'));
    }

    return (
        <label className={styles['switch']}>
            <input type="checkbox" checked={theme === 'light'} onChange={handleChange} />
            <div className={styles['switch-bg']} />
            <div className={styles['switch-dot']} />
        </label>
    )
}

export default ThemeSwitch;
