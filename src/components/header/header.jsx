import React from 'react';
import { Link } from 'react-router-dom';
import ThemeSwitch from './ThemeSwitch/ThemeSwitch';
import styles from './Header.module.scss';

function Header() {
    return (
        <header className={styles['main-header']}>
            <div className={`${styles['container']} container`}>
                <a href="#/" className={styles['title']}><strong>MOS'</strong>DevLOG</a>
                <div className={styles['middle-wrap']}>
                    <Link to="/category">文章分類</Link>|
                    <Link to="/article">Article</Link>|
                    <Link to="/article/2021-08-11-blog-startup">begining</Link>
                </div>
                <div className={styles['tools-wrap']}>
                    <ThemeSwitch />
                </div>
            </div>
        </header>
    )
}

export default Header;
