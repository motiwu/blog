import React, { useState, useEffect } from 'react';
import { HashRouter as Router, Switch, Route } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import routes from './routes';
import Header from './components/Header/Header';
import { setInit } from './reducer/commonSlice';
import './App.scss';

function App() {
    const dispatch = useDispatch();
    const [isInit, setIsInit] = useState(false); // 已取得初始化資料

    // 取得初始化資料
    useEffect(() => {
        fetch('/articles/init.json')
            .then(res => res.json())
            .then((result) => {
                dispatch(setInit(result));
                setIsInit(true);
            },
                (error) => {
                    console.warn('error', error);
                }
            )
    }, [dispatch, setIsInit]);

    if (!isInit) {
        return null;
    }
    return (
        <Router basename={process.env.PUBLIC_URL}>
            <Header />
            <div className="container">
                <main>
                    <Switch>
                        {routes.map((route, i) => (
                            <RouteWithSubRoutes key={i} {...route} />
                        ))}
                    </Switch>
                </main>
            </div>
        </Router>
    );
}

function RouteWithSubRoutes(route) {
    return (
        <Route
            path={route.path}
            render={props => (
                // pass the sub-routes down to keep nesting
                <route.component {...props} routes={route.routes} />
            )}
        />
    );
}

export default App;
