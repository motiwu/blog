import { createSlice } from '@reduxjs/toolkit';

const commonSlice = createSlice({
    name: 'common',
    initialState: {
        theme: 'light',
        articles: {},
        categories: {}
    },
    reducers: {
        setTheme: (state, action) => {
            state.theme = action.payload;
        },
        setInit: (state, action) => {
            state.articles = action.payload.articles;
            state.categories = action.payload.categories;
        },
        setTemplate: (state, action) => {
            state.articles[action.payload.id].template = action.payload.template;
        }
    },
});

export const {
    setTheme,
    setInit,
    setTemplate
} = commonSlice.actions;

export default commonSlice.reducer;
