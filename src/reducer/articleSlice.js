import { createSlice } from '@reduxjs/toolkit';

const articleSlice = createSlice({
    name: 'article',
    initialState: {
        templates: {},
        value: 0,
    },
    reducers: {
        setTemplates: (state, action) => {
            state.templates = action.payload
        }
    },
});

export const {
    setTemplates
} = articleSlice.actions;

export default articleSlice.reducer;
