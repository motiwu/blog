import { combineReducers } from 'redux';
import articleReducer from './articleSlice';
import commonReducer from './commonSlice';

export default combineReducers({
    article: articleReducer,
    common: commonReducer
});
