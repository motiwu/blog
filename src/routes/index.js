import Article from './Article';
import Category from './Category';

export const routes = [
    {
        path: "/article/:id?",
        component: Article
    },
    {
        path: "/category/:type?",
        component: Category
    },
    {
        path: "/tacos",
        component: Tacos,
        routes: [
            {
                path: "/tacos/bus",
                component: Bus
            },
            {
                path: "/tacos/cart",
                component: Cart
            }
        ]
    }
];
// function Sandwiches() {
//     return <h2>Sandwiches</h2>;
// }
  
  function Tacos({ routes }) {
    return (
      <div>
        <h2>Tacos</h2>
      </div>
    );
  }
  
  function Bus() {
    return <h3>Bus</h3>;
  }
  
  function Cart() {
    return <h3>Cart</h3>;
  }
export default routes;
