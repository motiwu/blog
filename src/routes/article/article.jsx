import React, { useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import ReactMarkdown from 'react-markdown';
// import { setTemplates } from '../../reducer/articleSlice';
import components from './components/CodeBlock/CodeBlock';
import { setTemplate } from '../../reducer/commonSlice';

function Article() {
    const store = useSelector((state) => state.common);
    const { articles } = store;
    const dispatch = useDispatch();
    const { id } = useParams();
    const history = useHistory();

    // 取得模板
    useEffect(() => {
        if (!articles[id]) {
            return;
        }
        const path = articles[id].path;
        if (!path) {
            history.push('/article');
            return;
        }
        fetch(path)
            .then(res => res.text())
            .then((template) => {
                dispatch(setTemplate({
                    id,
                    template
                }));
            },
                (error) => {
                    console.warn('error', error);
                }
            )
    }, [dispatch, history, articles, id]);

    if (!id || !articles[id]?.template) {
        return <div>LOADING || 文章不存在喔</div>;
    }

    return (
        <div className="markdown-body">
            <ReactMarkdown components={components}>{articles[id].template}</ReactMarkdown>
        </div>
    )
}

export default Article;
