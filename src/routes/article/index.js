import React, { Suspense } from 'react';

const ArticleComponent = React.lazy(() => import('./Article'));

function Article() {
    return (
        <Suspense fallback={<div>Loading...</div>}>
            <ArticleComponent />
        </Suspense>
    );
}

export default Article;
