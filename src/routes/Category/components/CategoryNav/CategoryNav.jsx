import React from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import styles from './CategoryNav.module.scss';

function CategoryNav() {
    const store = useSelector((state) => state.common);
    const { articles, categories } = store;

    // 文章列表中所有的分類
    const articleCategories = [...new Set(Object.keys(articles).map(item => articles[item].category))];

    return (
        <nav className={styles['wrap']}>
            <NavLink
                className={styles['link']}
                activeClassName={styles['active']}
                to={'/category/all'}
            >
                All
            </NavLink>
            {
                categories.map(category => {
                    if (!articleCategories.includes(category.key)) {
                        return null;
                    }
                    return(
                        <NavLink
                            key={category.key}
                            className={styles['link']}
                            activeClassName={styles['active']}
                            to={`/category/${category.key}`}
                        >
                            {category.name}
                        </NavLink>
                    )
                })
            }
        </nav>
    )
}

export default CategoryNav;
