import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './ArticleItem.module.scss';

function ArticleItem(props) {
    const { id, article } = props;
    console.warn(article);
    return (
        <section className={styles['wrap']}>
            <h4>{article.title}</h4>
            <p>{article.intro}</p>
            <NavLink className={styles['link']} to={`/article/${id}`}>more</NavLink>
        </section>
    )
}

export default ArticleItem;
