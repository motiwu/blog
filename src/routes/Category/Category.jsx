import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useParams, useHistory } from 'react-router-dom';
import CategoryNav from './components/CategoryNav/CategoryNav';
import ArticleItem from './components/ArticleItem/ArticleItem';
import styles from './Category.module.scss';

function Category() {
    const store = useSelector((state) => state.common);
    const [articleFilter, setArticleFilter] = useState([]); // 整理後的文章列表
    const { type } = useParams();
    const history = useHistory();
    const { articles } = store;

    // 未指定type則預選all
    useEffect(() => {
        if (!type) {
            history.push('/category/all');
        }
    }, [history, type]);

    // 依據所選分類，更新文章列表
    useEffect(() => {
        setArticleFilter(type === 'all' ? Object.keys(articles) : Object.keys(articles).filter(item => articles[item].category === type));
    }, [articles, type]);

    if (articleFilter.length < 1) {
        return <div>此分類無任何文章</div>;
    }

    return (
        <article className={styles['article-list-wrap']}>
            <CategoryNav />
            {articleFilter.map(id => (
                <ArticleItem key={id} id={id} article={articles[id]} />
            ))}
        </article>
    )
}

export default Category;
