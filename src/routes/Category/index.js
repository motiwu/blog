import React, { Suspense } from 'react';

const CategoryComponent = React.lazy(() => import('./Category'));

function Category() {
    return (
        <Suspense fallback={<div>Loading...</div>}>
            <CategoryComponent />
        </Suspense>
    );
}

export default Category;
